import React from 'react';

import './SingleSeries.css';

const SingleSeries = ({ title, image}) => {
    return (
        <div className="singleSeries">
                <div className="singleSeries__img">
                    <img className="singleSeries__img" src={image} alt='h'/>
                </div>
                <h3 className="singleSeries__title">{title}</h3>
        </div>
    )
}

export default SingleSeries
