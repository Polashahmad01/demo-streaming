import React, { useState, useEffect } from 'react';
import axios from 'axios';

import './SeriesPage.css';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import SingleSeries from './SingleSeries/SingleSeries';
import Loading from '../Loading/Loading';

const SeriesPage = () => {
    const [ loading, setLoading ] = useState(false);
    const [ series, setSeries ] = useState([]);

    const filteredSeries = series.filter((serie) => {
        return serie.programType === 'series';
    });

    const finalSeries = filteredSeries.splice(0, 21);

    const sortedFinalSeries = finalSeries.sort((a, b) => {
        if(a.title.toLowerCase() < b.title.toLowerCase()) {
            return -1;
        }
        if(a.title.toLowerCase() > b.title.toLowerCase()) {
            return 1;
        }
        return 0;
    });

    useEffect(() => {
        async function fetchData() {
            setLoading(true)
            const request = await axios.get('https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json');
            setSeries(request.data.entries)
            setLoading(false)
        }

        fetchData();
    }, [])

    return (
        <div className="seriesPage">
            <Navbar />
            <h2 className="seriesPage__title">
                Popular Series
            </h2>
            <div className="seriesPage__signleSeries">
                {loading ? <Loading /> : null}
            { sortedFinalSeries.map(sortedFinalSerie => {
                return <SingleSeries
                    key={sortedFinalSerie.title} 
                    title={sortedFinalSerie.title}
                    image={sortedFinalSerie.images['Poster Art'].url}
                />
            })}
            
            </div>
            <Footer />
        </div>
    )
}

export default SeriesPage
