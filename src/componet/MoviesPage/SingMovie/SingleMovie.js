import React from 'react';

import './SingleMovie.css';

const SingleMovie = ({ image, title }) => {
    return (
        <div className="singleMovie">
            <div className="singleMovie__imgContainer">
               <img className="singleMovie__img" src={image} alt='h'/>
               <h3 className="singleMovie__title">{title}</h3>
           </div>  
        </div>
    )
}
export default SingleMovie