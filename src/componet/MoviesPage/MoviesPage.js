import React, { useState, useEffect } from 'react';
import axios from 'axios';

import './MoviesPage.css';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';
import SingleMovie from './SingMovie/SingleMovie';
import Loading from '../Loading/Loading';

const MoviesPage = () => {
    const [ loading, setLoading ] = useState(false);
    const [ movies, setMovies ] = useState([]);

    const filteredMovies = movies.filter((movie) => {
        return movie.programType === 'movie' && movie.releaseYear >= 2010;
    })

    const finalMovies = filteredMovies.splice(0, 21);

    const sortedMovies = finalMovies.sort((a, b) => {
        if(a.title.toLowerCase() < b.title.toLowerCase()) {
            return -1;
        }
        if(a.title.toLowerCase() > b.title.toLowerCase()) {
            return 1;
        }
        return 0;
    });

    useEffect(() => {
        async function fetchData() {
                        setLoading(true)
                        const request = await axios.get('https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json');
                        setMovies(request.data.entries)
                        setLoading(false)
        }
        fetchData();
    }, [])

    return (
        <div className="moviesPage">
            <Navbar />
            <h2 className="moviesPage__title">
                Popular Movies
            </h2>
            <div className="moviesPage__singleMovile">
                {loading ? <Loading /> : null}
                {sortedMovies.map(sortedMovie => {
                    return <SingleMovie 
                                key={sortedMovie.title}
                                title={sortedMovie.title}
                                image={sortedMovie.images['Poster Art'].url}
                            />
                })}
            </div>
            <Footer />
        </div>
    )
}

export default MoviesPage
