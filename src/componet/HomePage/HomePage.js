import React from 'react';
import { Link } from 'react-router-dom';

import './HomePage.css';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';

const HomePage = () => {
    return (
        <div className="homePage">
            <Navbar />
            <h2 className="homePage__title">
                Popular Titles
            </h2>
            <div className="homePage__boxContainer">
                <Link to="/series">
                    <div className="homePage__box">
                        <div>
                            Series
                        </div>
                    </div>
                    <span className="homePage__boxSubtitle">Popular Series</span>
                </Link>
                <Link to="/movies">
                    <div className="homePage__box2">
                        <div>
                            Movies
                        </div>
                    </div>
                    <span className="homePage__boxSubtitle">Popular Movies</span>
                </Link>
            </div>
            <Footer />
        </div>
    )
}

export default HomePage
