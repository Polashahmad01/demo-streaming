import React from 'react';
import { Link }  from 'react-router-dom';

import './Navbar.css';

const Navbar = () => {
    return (
        <div className="navbar">
            <Link to="/"><h1 className="navbar__logo">DEMO Streaming</h1></Link>
            <nav className="navbar__list">
                <ul className="navbar__links">
                    <li>
                        <Link to="#" className="navbar__login">Login</Link>
                    </li>
                    <li className="navbar__freeTrialBtn">
                        <Link to="#">Start your free trial</Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Navbar
