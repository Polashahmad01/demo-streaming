import React from 'react';
import { Link } from 'react-router-dom';

import './Footer.css';
import facebook from '../../assets/social/facebook-white.svg';
import twitter from '../../assets/social/twitter-white.svg';
import instagram from '../../assets/social/instagram-white.svg';
import AppleStoreIcon from '../../assets/store/app-store.svg';
import PlayStoreIcon from '../../assets/store/play-store.svg';
import WindowsIcon from '../../assets/store/windows-store.svg';

const Footer = () => {
    return (
        <div className="footer">
            <div className="footer__top">
                <div className="footer__topLinks">
                    <ul className="footer__topLinksList">
                        <li>
                            <span><Link to="#">Home</Link> &nbsp;&nbsp;|&nbsp;</span>
                        </li>
                        <li>
                            <span><Link className="footer__topLink" to="#">Terms and Conditions</Link> &nbsp;&nbsp;|&nbsp;</span>
                        </li>
                        <li>
                            <span><Link className="footer__topLink" to="#">Privacy Policy</Link> &nbsp;&nbsp;|&nbsp;</span>
                        </li>
                        <li>
                            <span><Link className="footer__topLink" to="#">Collection Statement</Link> &nbsp;&nbsp;|&nbsp;</span>
                        </li>
                        <li>
                            <span><Link className="footer__topLink" to="#">Help</Link> &nbsp;&nbsp;|&nbsp;</span>
                        </li>
                        <li>
                            <span><Link className="footer__topLink" to="#">Manage Account</Link></span>
                        </li>
                    </ul>
                </div>
                <div className="footer__topCopyright">
                    <p>Copyright &copy; 2016 DEMO Streaming. All Rights Reserved.</p>
                </div>
            </div>
            <div className="footer__bottom">
                <div className="footer__bottomLeft">
                    <ul>
                        <li>
                            <Link to="#">
                                <img className="footer__bottomLeftSocialIcon" src={facebook} alt="facebook-icon"/>
                            </Link>
                        </li>
                        <li>
                            <Link to="#">
                                <img className="footer__bottomLeftSocialIcon" src={twitter} alt="twitter-icon"/>
                            </Link> 
                        </li> 
                        <li> 
                            <Link to="#"> 
                                <img className="footer__bottomLeftSocialIcon" src={instagram} alt="instagram-icon"/>
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="footer__bottomRight">
                   <ul>
                        <li>
                            <Link to="#">
                                <img className="footer__bottomRightStoreIcon" src={AppleStoreIcon} alt="appleStore-icon"/>
                            </Link>
                        </li>
                        <li>
                            <Link to="#">
                                <img className="footer__bottomRightStoreIcon" src={PlayStoreIcon} alt="playStore-icon"/>
                            </Link>
                        </li>
                        <li>
                            <Link to="#">
                                <img className="footer__bottomRightStoreIcon" src={WindowsIcon} alt="windows-icon"/>
                            </Link>
                        </li>
                   </ul>
                </div>
            </div>
        </div>
    )
}

export default Footer
