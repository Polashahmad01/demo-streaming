import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.css';
import HomePage from './componet/HomePage/HomePage';
import SeriesPage from './componet/SeriesPage/SeriesPage';
import MoviesPage from './componet/MoviesPage/MoviesPage';

function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/movies">
            <MoviesPage/>
          </Route>
          <Route path="/series">
            <SeriesPage/>
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
