# **DEMO Streaming**

## To start the development server please follow the steps below.


- **For Setup Packages:** `npm-install`

- **To Start The Development Server:**  `npm-start`


#
**This site is live here:** https://demostreamings.netlify.app
#
 ### **How did you decide on the technical and architectural choices used as part of your solution?**

 - First of all, I decided to make the architectural structure of my solution. Which helped me divide the whole project into different reusable components, such as: **Navbar, Footer, HomePage, MoviesPage, SeriesPage** 

 - For the technical choice I did UI/UX design then I added the functionality one after another.

 - Technologies I use: **React, React-roter-dom, React Hooks, HTML5, CSS3, JavaScript, Axios, etc**


### **Are there any improvements you could make to your submission?**

- Yes, I can make a lot of improvements in this project, such as: **Improving the design of UI/UX, Adding logout-logout functionality, Adding some cool animations, Adding more data on movies & series, etc**


### **What would you do differently if you were allocated more time?**

- I would like to do inprovements on this project such as: **Improving the design of UI/UX, Adding logout-logout functionality, Adding some cool animations, Adding more data on movies & series, etc**

#
**Thanks**